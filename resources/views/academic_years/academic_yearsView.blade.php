@extends('index')
 
@section('content')
<div class="col-md-8 col-md-offset-2">
    <h3>Data Academic Year</h3>   
    <table class="table" id="data">
        <thead>
            <tr>
                <td>Nama</td>
                <td>Stok</td>
                <td>Stok</td>
            </tr>
        </thead>
    </table>
</div>
@endsection
 
@push('js')
<script type="text/javascript">
    $(function(){
        $("#data").DataTable({
            "scrollX": true,
             processing: true,
                serverSide: true,
                ajax: '{{ url("get-academic_years") }}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'odd_year', name: 'odd_year' },
                    { data: 'even_year', name: 'even_year' },
                ]
        });
    });
</script>
@endpush