@extends('index')
 
@section('content')
<div class="col-md-8 col-md-offset-2">
    <h3>Data Barang</h3>   
    <table class="table" id="data-barang">
        <thead>
            <tr>
                <td>Nama</td>
                <td>Stok</td>
            </tr>
        </thead>
    </table>
</div>
@endsection
 
@push('js')
<script type="text/javascript">
    $(function(){
        $("#data-barang").DataTable({
             processing: true,
                serverSide: true,
                ajax: '{{ url("get-barang") }}',
                //type:post,
                //datatype: json,
                columns: [
                    { data: 'nama', name: 'nama' },
                    { data: 'stok', name: 'stok' },
                ]
        });
    });
</script>
@endpush