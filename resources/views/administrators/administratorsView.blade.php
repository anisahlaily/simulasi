@extends('index')
 
@section('content')
<div class="row">
<div class="col-md-8 col-md-offset-2">
    <h3 align="center">Data Academic Year</h3> 
    <a href="{{route('administratorsCreate')}}" class="btn btn-primary btn-md" ><span  class="glyphicon glyphicon-list-alt"></span>  Add New</a></a>  
    <div>
        <h3> </h3>
    </div>
    <table class="table" id="data-administrators">
        <thead>
            <tr>
                <td>Nama</td>
            </tr>
        </thead>
    </table>
</div>
</div>
@endsection
 
@push('js')
<script type="text/javascript">
    $(function(){
        $("#data-administrators").DataTable({
            "scrollX": true,
             processing: true,
                serverSide: true,
                ajax: '{{ url("get-administratorsView") }}',
                columns: [
                    { data: 'user_id', name: 'user_id' },
                ]
        });
    });
</script>
@endpush