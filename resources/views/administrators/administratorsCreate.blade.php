@extends('index')

@section('content')
<section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            <h1>Tambah Administrator</h1>
            <hr>
            <form action="{{route('administratorsStore')}}" method="post">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <select name="user_id" id="user_id" class="form-control">
                                @foreach($users as $user)
                                <option value="{{ $user->id}}">{{ $user->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
                        </div>
               
            
                <div class="form-group">
                    <button type="save" name="save" id="save" class="btn btn-md btn-primary">Submit</button>
                    <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection