<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');

});

    Route::resource('administrator', 'administratorsController');

    Route::resource('mahasiswa_calons', 'mahasiswa_calonController');
    Route::get('mahasiswa_calon/',['as' => 'mahasiswa_calon', 'uses' => 'mahasiswa_calonController@index']);
    Route::get('mahasiswa_calonCreate/',['as' => 'mahasiswa_calonCreate', 'uses' => 'mahasiswa_calonController@create']);
    //Route::get('get-mahasiswa_calon','mahasiswa_calonController@getmahasiswa_calon');
    

    Route::resource('majors', 'major\majorController');
    Route::get('majorsIndex/',['as' => 'majorsIndex', 'uses' => 'major\majorController@index']);
    Route::get('majorsCreate/',['as' => 'majorsCreate', 'uses' => 'major\majorController@create']);

    Route::resource('faculties', 'faculties\facultiesController');
    Route::get('facultiesIndex/',['as' => 'facultiesIndex', 'uses' => 'faculties\facultiesController@index']);
    Route::get('facultiesCreate/',['as' => 'facultiesCreate', 'uses' => 'faculties\facultiesController@create']);

    Route::resource('departments', 'departments\departmentsController');
    Route::get('departmentsIndex/',['as' => 'departmentsIndex', 'uses' => 'departments\departmentsController@index']);
    Route::get('departmentsCreate/',['as' => 'departmentsCreate', 'uses' => 'departments\departmentsController@create']);

    Route::get('academic_years/',['as' => 'academic_years', 'uses' => 'academic_yearsController@index']);
    Route::get('get-academic_years','academic_yearsController@getacademic_years');

    Route::get('administrators/',['as' => 'administrators', 'uses' => 'administratorsController@index']);
    Route::get('get-administratorsView','administratorsController@getadministrators');
    Route::get('get-administratorsCreate','administratorsController@create');
    Route::get('administratorsCreate/',['as' => 'administratorsCreate', 'uses' => 'administratorsController@create']);
    Route::get('administratorsStore/',['as' => 'administratorsStore', 'uses' => 'administratorsController@store']);