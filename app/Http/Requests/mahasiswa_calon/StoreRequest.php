<?php

namespace App\Http\Requests\mahasiswa_calon;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            'program_studi_id' => 'Required',
            'tahun_ajaran_id' => 'Required',
            'nama' => 'Required',
            'telp' => 'Required',
            'email' => 'Required',
            'tempat_lahir' => 'Required',
            'gender' => 'Required',
            'alamat_asal' => 'Required',
            'jalur_pendaftaran' => 'Required',
            'reguler' => 'Required',
            'hasil_seleksi' => 'Required'
        ];
    }

    public function messages()
    {
        return [
            'program_studi_id.required' => 'Tidak Boleh Kosong.',
            'tahun_ajaran_id.required' => 'Tidak Boleh Kosong.',
            'nama.required' => 'Tidak Boleh Kosong.',
            'telp' => 'Tidak Boleh Kosong.',
            'email.required' => 'Tidak Boleh Kosong.',
            'tempat_lahir.required' => 'Tidak Boleh Kosong.',
            'gender.required' => 'Tidak Boleh Kosong.',
            'alamat_asal.required' => 'Tidak Boleh Kosong.',
            'jalur_pendaftaran.required' => 'Tidak Boleh Kosong.',
            'reguler.required' => 'Tidak Boleh Kosong.',
            'hasil_seleksi.required' => 'Tidak Boleh Kosong.'
        ];
    
    }
}
