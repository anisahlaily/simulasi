<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\academic_years;
use Datatables;

class academic_yearsController extends Controller
{
    public function index()
    {
        return view('academic_years.academic_yearsView');
    }
 
    public function getacademic_years()
    {
        $academic_years = academic_years::select(['name', 'odd_year','even_year'])->get();
  
        return Datatables::of($academic_years)->make(true);
    }
}
