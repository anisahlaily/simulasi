<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\administrators;
use App\User;
use App\Http\Requests;
use App\Http\Requests\administrators\StoreRequest;
use App\Http\Requests\administrators\UpdateRequest;
//use App\Http\Requests\administrators\StoreRequestadministratorsuser;
use Datatables;

class administratorsController extends Controller
{
    public function index()
    {
        return view('administrators.administratorsView');
    }
 
    public function getadministrators()
    {
        $administrators = administrators::select(['user_id'])->get();
  
        return Datatables::of($administrators)->make(true);
    }
    public function create()
    {
    	$users = User::all();
        return view('administrators.administratorsCreate', compact('users'));
    }
    public function store(Request $request)
   {
       $data = new administrators();
       $data->user_id = $request->user_id;
       $data->save();

       return redirect()->route('administrator.index');
   }

}
