<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang; /* Import model barang */
use Datatables; /* Import datatables-nya juga */

class DatatablesController extends Controller
{
    public function index()
    {
        return view('DTindex');
    }
 
    public function getBarang()
    {
        $barang = Barang::select(['nama', 'stok'])->get();
  
        return Datatables::of($barang)->make(true);
    }
}
