<?php

namespace App\Http\Controllers\faculties;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\faculties;
use App\kota_kabupaten;

class facultiesController extends Controller
{
    public function index()
    {   
        
    	$faculties=faculties::all();
        return view('faculties.facultiesView',compact('faculties'));
    }
    public function create()
    {
    	$kota_kabupaten=kota_kabupaten::all();
        return view('faculties.facultiesCreate',compact('kota_kabupaten'));
    }
    public function store(Request $request)
    {
        $faculties = new faculties();
        $faculties->name=$request->name;
        $faculties->university=$request->university;
        $faculties->telp=$request->telp;
        $faculties->email=$request->email;
        $faculties->founded_on=$request->founded_on;
        $faculties->city=$request->city;
        $faculties->save();
        return redirect()->route('faculties.index')->with('alert-success','Data berhasil Disimpan.');
    }
    public function edit($id)
    {
        $kota_kabupaten=kota_kabupaten::all();
        $faculties = faculties::findOrFail($id);
        return view('faculties.facultiesEdit', compact('kota_kabupaten','faculties'));
    }
    public function update(Request $request, $id)
    {
        $faculties = faculties::findOrFail($id);
        $faculties->name=$request->name;
        $faculties->university=$request->university;
        $faculties->telp=$request->telp;
        $faculties->email=$request->email;
        $faculties->founded_on=$request->founded_on;
        $faculties->city=$request->city;
        $faculties->save();
        return redirect()->route('faculties.index')->with('alert-success', 'Data Successfully Updated.');
    }
    public function destroy($id)
    {
        $faculties = faculties::findOrFail($id);
        $faculties->delete();
        return redirect()->route('faculties.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
