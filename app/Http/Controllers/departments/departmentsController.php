<?php

namespace App\Http\Controllers\departments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\department;
use App\kota_kabupaten;
use App\faculties;
use App\User;

class departmentsController extends Controller
{
    public function index()
    {   
    	
    	$departments=department::all();
        return view('departments.departmentsView',compact('departments'));
    }
    public function create()
    {
    	$users=User::all();
        $kota_kabupaten=kota_kabupaten::all();
    	$faculties=faculties::all();
        return view('departments.departmentsCreate',compact('faculties','kota_kabupaten','users'));
    }
    public function store(Request $request)
    {
        $departments = new department();
        $departments->faculty_id=$request->faculty_id;
        $departments->name=$request->name;
        $departments->telp=$request->telp;
        $departments->email=$request->email;
        $departments->address=$request->address;
        $departments->facsimile=$request->facsimile;
        $departments->homepage=$request->homepage;
        $departments->faculty_name=$request->faculty_name;
        $departments->university=$request->university;
        $departments->city=$request->city;
        $departments->user_id=$request->user_id;
        $departments->save();
        return redirect()->route('departments.index')->with('alert-success','Data berhasil Disimpan.');
    }
    public function edit($id)
    {
        $users=User::all();
        $departments = department::findOrFail($id);
        $kota_kabupaten=kota_kabupaten::all();
    	$faculties=faculties::all();
        return view('departments.departmentsEdit',compact('faculties','kota_kabupaten','users','departments'));
    }
    public function update(Request $request, $id)
    {
        $departments = department::findOrFail($id);
        $departments->faculty_id=$request->faculty_id;
        $departments->name=$request->name;
        $departments->telp=$request->telp;
        $departments->email=$request->email;
        $departments->address=$request->address;
        $departments->facsimile=$request->facsimile;
        $departments->homepage=$request->homepage;
        $departments->faculty_name=$request->faculty_name;
        $departments->university=$request->university;
        $departments->city=$request->city;
        $departments->user_id=$request->user_id;
        $departments->save();
        return redirect()->route('departments.index')->with('alert-success', 'Data Successfully Updated.');
    }
    public function destroy($id)
    {
        $departments = department::findOrFail($id);
        $departments->delete();
        return redirect()->route('departments.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
