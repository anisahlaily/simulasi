<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('barang')->insert(
            [
                [
                    'nama'          => 'Rinso Cair',
                    'stok'      => '30',
                ],
                [
                    'nama'          => 'Rinso Bubuk',
                    'stok'      => '30',
                ],
                [
                    'nama'          => 'hape Jadul',
                    'stok'      => '20',
                ],
            ]
        );
    }
}
