<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerPembimbinganAkademikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_pembimbingan_akademik', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->integer('lecturer_id')->unsigned()->index();
            $table->integer('jumlah_mahasiswa');
            $table->float('rata_pertemuan');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
            $table->foreign('lecturer_id')->references('id')->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_pembimbingan_akademik');
    }
}
