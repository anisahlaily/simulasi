<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorTracerFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_tracer_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->integer('period_id')->unsigned()->index();
            $table->string('instansi');
            $table->string('alamat');
            $table->string('cp');
            $table->string('cp_telp');
            $table->tinyInteger('integritas');
            $table->tinyInteger('keahlian');
            $table->tinyInteger('bahasa_inggris');
            $table->tinyInteger('pengguna_it');
            $table->tinyInteger('komunikasi');
            $table->tinyInteger('kerjasama_tim');
            $table->tinyInteger('pengembangan_diri');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_tracer_feedbacks');
    }
}
