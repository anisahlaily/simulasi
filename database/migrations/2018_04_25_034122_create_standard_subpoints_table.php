<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardSubpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_subpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('point_id')->unsigned()->index();
            $table->string('subpoint');
            $table->text('description');
            $table->text('very_good_tooltip');
            $table->text('good_tooltip');
            $table->text('middle_tooltip');
            $table->text('bad_tooltip');
            $table->text('very_bad_tooltip');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('point_id')->references('id')->on('standard_points');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_subpoints');
    }
}
