<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaCalonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_calon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_studi_id');
            $table->integer('tahun_ajaran_id');
            $table->string('nama');
            $table->string('telp');
            $table->string('email');
            $table->integer('tempat_lahir');
            $table->tinyInteger('gender');
            $table->string('alamat_asal');
            $table->tinyInteger('jalur_pendaftaran');
            $table->tinyInteger('reguler');
            $table->tinyInteger('hasil_seleksi');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_calon');
    }
}
