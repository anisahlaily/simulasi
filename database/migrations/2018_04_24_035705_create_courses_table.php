<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->string('nama');
            $table->string('kode');
            $table->string('semester');
            $table->string('semester_urut');
            $table->float('bobot_sks');
            $table->string('pilihan');
            $table->integer('sks_inti');
            $table->integer('sks_institusional');
            $table->integer('bobot_tugas');
            $table->string('kel_deskripsi');
            $table->string('kel_silabus');
            $table->string('kel_sap');
            $table->string('penyelenggara');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
