<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->string('test_number');
            $table->string('name');
            $table->string('nim');
            $table->string('telp');
            $table->string('email');
            $table->enum('gender',array('M','F'));
            $table->integer('city_id')->unsigned()->index();
            $table->string('address');
            $table->string('place_of_birth');
            $table->date('date_of_birth');
            $table->string('old_school');
            $table->enum('selection_type',array('JU','BM','G1','G2','G3'));
            $table->tinyInteger('transfer');
            $table->tinyInteger('regular');
            $table->enum('selection_status',array('L','TL'));
            $table->enum('registration_status',array('R','NR'));
            $table->enum('student_status',array('A','DO','MDO'));
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
            $table->foreign('city_id')->references('id')->on('kota_kabupaten');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_students');
    }
}
