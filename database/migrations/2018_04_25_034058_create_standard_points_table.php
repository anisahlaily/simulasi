<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_id')->unsigned()->index();
            $table->string('point');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('standard_id')->references('id')->on('standards');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_points');
    }
}
