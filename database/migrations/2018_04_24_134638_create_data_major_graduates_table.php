<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorGraduatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_graduates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->double('ipk');
            $table->date('graduation_date');
            $table->integer('study_period');
            $table->integer('thesis_completion_time');
            $table->tinyInteger('have_worked');
            $table->integer('work_waiting_time');
            $table->string('working_institution');
            $table->string('working_address');
            $table->tinyInteger('suitability_of_work');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_id')->references('id')->on('data_major_students');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_graduates');
    }
}
