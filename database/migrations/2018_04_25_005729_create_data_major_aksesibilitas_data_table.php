<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorAksesibilitasDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_aksesibilitas_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->enum('jenis',array('MAHASISWA','KRS','JADWAL_MK','NILAI_MK','TRANSKIP','LULUSAN','DOSEN','PEGAWAI','KEUANGAN','INVENTARIS','SPP'));
            $table->integer('pengelolaan_data');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_aksesibilitas_data');
    }
}
