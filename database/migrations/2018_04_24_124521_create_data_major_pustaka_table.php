<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorPustakaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_pustaka', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->enum('kategori',array('BUKU_TEKS','JURNAL_NASIONAL_T','JURNAL_INTERNASIONAL','PROSIDING','SKRIPSI_TESIS','DISERTASI'));
            $table->integer('jumlah_judul');
            $table->integer('jumlah_copy');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_pustaka');
    }
}
