<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->text('mekanisme_penyusunan');
            $table->text('visi');
            $table->text('misi');
            $table->text('tujuan');
            $table->text('sasaran');
            $table->text('strategi_pencapaian');
            $table->text('sosialisasi');
            $table->text('kompetensi_utama_lulusan');
            $table->text('kompetensi_pendukung_lulusan');
            $table->text('kompetensi_pilihan_lulusan');
            $table->integer('sks_pilihan_wajib');
            $table->enum('penyelesaian_tugas_akhir',array('1','2'));
            $table->integer('total_mhs_ft');
            $table->integer('total_mhs_ft_m1');
            $table->integer('total_mhs_ft_m2');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_profiles');
    }
}
