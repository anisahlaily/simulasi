<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subpoint_id')->unsigned()->index();
            $table->integer('period_id')->unsigned()->index();
            $table->string('name');
            $table->enum('type',array('document','image','url','text','same_as'));
            $table->text('attachment');
            $table->integer('same_as_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('subpoint_id')->references('id')->on('standard_subpoints');
            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_attachments');
    }
}
